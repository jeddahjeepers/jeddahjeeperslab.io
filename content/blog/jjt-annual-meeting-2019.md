---
title: "الاجتماع السنوي لأعضاء فريق جدة جيبرز"
date: 2019-02-14T20:00:00+03:00
image: images/blog/jjt-2019-annual-meeting.jpg
author: يوسف رفه
description : "اجتماع أعضاء فريق جدة جيبرز السنوي"
lastmod: 2019-02-28T21:00:00+03:00
---

عقد فريق جدة جيبرز اجتماعه السنوي يوم **الخميس** *14 فبراير 2019* في استضافة [MPC](https://www.instagram.com/mpcraied/). هذا وحضر الاجتماع معظم أعضاء الفريق باستثناء البعض نظرا لظروفهم الخاصة.

بدأ الاجتماع بذكر من كتاب الله الكريم ثم كلمة من رئيس الفريق الكابتن/ **سليمان البطحي** وضح فيها توجه الفريق لهذا العام تحت شعار **كُن مُبادرًا**


ومن ثم تم التطرق لعدة مواضيع مختلفة واستهل بأجندة الاجتماع التي كان من أبرزها:

* الترحيب بانضمام الأعضاء الجدد في الفريق
* استعراض سريع لأهم إنجازات الفريق لعام 2018
* انتخاب إدارة الفريق لعام 2019
* استعراض مخطط برامج عام 2019
* مناقشة مواضيع مختلفة تم طرحها من قبل الأعضاء
* الختام وتناول وجبة العشاء
